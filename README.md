# Solar limb detection

Python routines for finding the solar limb on Ca II and H-$\alpha$ observations. Primarily, in this package, we focus on two main components of interacting with the solar limb. These are:

1) Detecting the solar limb, thereby calculating the centre $x, y$ pixel location, and the size of the apparant radius.
2) Removal of the limb-darkening effect to allow you to perform solar activity detection without needing to take into account this effect.

## Getting started

You can install this package directly from this git repository using pip:

```bash
pip install git+https://gitlab.lis-lab.fr/presage/solar-limb-detection.git
```

### Limb Detection

We shall first look at the process of detecting the location and size of the solar limb. To begin, import the `fit_limb` function from this package:

```python
from solar_limb_detection import fit_limb
```

This function is pretty simple, pass the image (as a numpy or sunpy map) as an argument, and receive a `SolarLimb` object as a result. This `SolarLimb` object contains the following class variables:

- `cx` - the centre-x pixel location.
- `cy` - the centre-y pixel location.
- `radius` - the apparent radius in pixel values.

We may use this functionality in the following way:

```python
# first read the file into separate numpy data and header arguments
data, header = sunpy.io.read_file('/path/to/fits-file.fits')[0]
limb = fit_limb(data)  # determine the size and location of the solar limb

# plot the data, overlaying the location of the solar limb as a red circle.
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
fig, ax = plt.subplots()
ax.imshow(data, cmap="gray")
ax.add_patch(Circle((limb.cx, limb.cy), limb.radius, fill=None, edgecolor="red"))
ax.invert_yaxis()
```

Following this as a guide, you should end up with something like:

![example of the detected solar limb](images/detection.png)

### Removal of limb-darkening

The removal of the limb-darkening effect is very similar to the usage of the limb detection, except that it requires a sunpy map due to the dependancy on much of the meta data of the fits file. The fits file requires the following information for this functionality to work:

- `crpix1`/`crpix2` -- the centre pixel in the 1st & 2nd dimension of the data. This can be gained through the limb detection as above.
- `rsun_obs` -- the size of the sun in the observation which again is determined by the limb detection.
- `wavelnth` -- the wavelength of the observation in angstroms.

Providing these meta data are available, we can remove the limb-darkening effect via:

```python
from solar_limb_detection import remove_limb_darkening  # import the function

smap = remove_limb_darkening(smap)  # call the function, passing a sunpy map 
# and returning a new sunpy map
```

## Test scripts

As further and more explanatory examples, we have written two test scripts. These are:

 - `tests/test_solar_limb_darkening.py`
 - `tests/test_solar_limb_detection.py`

Please see these scripts for comprehensive guides on using this library in your own works.

## References

Reference implementation of the solar limb detection can be seen in:

This method of detecting the solar limb is demonstrated in Pötzi et al. Kanzelhöhe Observatory: Instruments, Data Processing and Data Products. Solar Physics 296, no. 11 (November 2021): 164. https://doi.org/10.1007/s11207-021-01903-4.

With the method originating in:

Taubin, G. Estimation of Planar Curves, Surfaces, and Nonplanar Space Curves Defined by Implicit Equations with Applications to Edge and Range Image Segmentation. IEEE Transactions on Pattern Analysis and Machine Intelligence 13, no. 11 (November 1991): . https://doi.org/10.1109/34.103273.

For the removal of the limb-darkening effect, we use the constants values presented in:

Cox, Arthur N., ed. Allen's Astrophysical Quantities. New York, NY: Springer, 2002. https://doi.org/10.1007/978-1-4612-1186-0.