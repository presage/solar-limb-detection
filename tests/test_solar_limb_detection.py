from solar_limb_detection import fit_limb
import sunpy
from pathlib import Path
import urllib.request


def download(url, output):
    def progress(block_num, block_size, total_size):
        if block_num % 10 == 0:
            down_size = f"{(block_num*block_size)/1000000000:.4f}/{total_size/1000000000:.4f} GB"
            perc_size = f"{round(100*((block_num*block_size)/total_size), 4):.4f}"
            print(
                f"Downloading {Path(url).name}: {down_size} ({perc_size}%)",
                end="\r",
                flush=True,
            )
    urllib.request.urlretrieve(url, output, progress)
    return output


# download the H-alpha test file
fn = download("https://bass2000.obspm.fr/bass2000/data//pub/sidc/Halpha/2306/UCH20230605064459.fts", "/tmp/halpha.fits")

# open the sunpy map file, reading in the data and header separately (so as to avoid errors)
data, header = sunpy.io.read_file(fn)[0]

# using the data, we can fit the solar limb to this image:
limb = fit_limb(data)
#SolarLimb(cx=1023.9826049804688, cy=1023.9387817382812, radius=907.2899780273438)

assert limb.radius == 907.2899780273438
assert limb.cx == 1023.9826049804688
assert limb.cy == 1023.9387817382812

# this will give us a class of SolarLimb with the class variables:
# - cx: the solar centre x
# - cy: the solar centre y
# - radius: the solar radius

# then, if we like, we can plot the data, overlaying the detected solar limb.
# import matplotlib.pyplot as plt
# from matplotlib.patches import Circle

# fig, ax = plt.subplots()
# ax.imshow(data, cmap="gray")
# ax.add_patch(Circle((limb.cx, limb.cy), limb.radius, fill=None, edgecolor="red"))
# ax.invert_yaxis() # correct flip in matplotlib