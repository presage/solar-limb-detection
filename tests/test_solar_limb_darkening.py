from solar_limb_detection import remove_limb_darkening
import sunpy
from pathlib import Path
import urllib.request


def download(url, output):
    def progress(block_num, block_size, total_size):
        if block_num % 10 == 0:
            down_size = f"{(block_num*block_size)/1000000000:.4f}/{total_size/1000000000:.4f} GB"
            perc_size = f"{round(100*((block_num*block_size)/total_size), 4):.4f}"
            print(
                f"Downloading {Path(url).name}: {down_size} ({perc_size}%)",
                end="\r",
                flush=True,
            )
    urllib.request.urlretrieve(url, output, progress)
    return output


# download the H-alpha test file
fn = download("https://bass2000.obspm.fr/bass2000/data//pub/sidc/Halpha/2306/UCH20230605064459.fts", "/tmp/halpha.fits")

# open the sunpy map file, reading in the data and header separately (so as to avoid errors)
smap = sunpy.map.Map("/tmp/halpha.fits")

# and remove the limb-darkening effect:
smap = remove_limb_darkening(smap)

# and optionally plot the data:
smap.peek()