from distutils.core import setup
from setuptools import find_packages

with open("requirements.txt") as f:
    required_packages = f.read().splitlines()

setup(
    name="solar_limb_detection",
    version="1.0",
    description="Python routines for finding the solar limb on Ca II and H-$\alpha$ observations.",
    author="Jay Paul Morgan",
    author_email="jay.morgan@univ-tln.fr",
    url="https://gitlab.lis-lab.fr/presage/solar-limb-detection",
    packages=find_packages(exclude=["tests"]),
    install_requires=required_packages)