import numpy as np
import sunpy
import sunpy.map
from typing import Union


def compute_dist_grid(smap) -> np.ndarray:
    """Create a 2D matrix where the values in this matrix
    are the distances of point i,j from the solar centre"""
    radius = (smap.rsun_obs / smap.scale.axis1).value
    centre_x, centre_y = smap.meta["crpix1"], smap.meta["crpix2"]
    xdim, ydim = smap.dimensions.x.to_value(), smap.dimensions.y.to_value()
    x_grid, y_grid = np.meshgrid(np.arange(xdim), np.arange(ydim), sparse=True)
    x_2 = (x_grid - centre_x) ** 2
    y_2 = (y_grid - centre_y) ** 2
    dist_grid = np.sqrt(x_2 + y_2) / radius
    dist_grid[np.where(dist_grid > 1)] = 0
    return dist_grid


def remove_limb_darkening(smap: sunpy.map.GenericMap) -> sunpy.map.GenericMap:
    data = smap.data.copy()
    dist = compute_dist_grid(smap)
    data = limb_normalisation(data, dist, smap.meta["wavelnth"])
    return sunpy.map.Map(data, smap.meta)


def _dark_limb(wavelength: int, dtype: str):
    # assert 4000 < wavelength < 15000

    # coefficients for 5th order poly fit to limb darkening
    # constant u.  Fit was done on data from Astrophysical
    # Quantities by Allen.

    if dtype == "u":
        a = -8.9829751
        b = 0.0069093916
        c = -1.8144591e-6
        d = 2.2540875e-10
        e = -1.3389747e-14
        f = 3.0453572e-19
    else:
        a = 9.2891180
        b = -0.0062212632
        c = 1.5788029e-6
        d = -1.9359644e-10
        e = 1.1444469e-14
        f = -2.599494e-19

    a = np.array([a, b, c, d, e, f])
    wavelength_series = np.array([wavelength**n for n in range(len(a))])
    ul = sum(a * wavelength_series)
    return ul


def limb_normalisation(arr: np.ndarray, dist_grid: np.ndarray, wavelength: int):
    """Remove limb darkening using a 5th polynomial with fitted constants
    from Allen
    """
    ul = _dark_limb(wavelength, dtype="u")
    vl = _dark_limb(wavelength, dtype="v")
    limbfilt = (
        1
        - ul
        - vl
        + ul * np.cos(np.arcsin(dist_grid))
        + vl * np.cos(np.arcsin(dist_grid)) ** 2
    )
    return arr / limbfilt
